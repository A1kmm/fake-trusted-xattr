# fake-trusted-xattr

Allows you to run programs that rely on setting trusted Linux extended attributes
without needing special permissions like `CAP_SYS_ADMIN`.

fake-trusted-xattr achieves this by hooking calls into libc to get or set xattr
through a dynamic library you can load with `LD_PRELOAD`, and transparently
translating  attribute names starting with `trusted.` to `user.tr.`. Note that this means that programs which
have been previously been allowed to set trusted xattrs won't see those xattrs any
more - and these attributes won't actually impact features such as SELinux.
fake-trusted-xattr is therefore most useful for programs that use xattrs only for
their own internal purposes, and when you are doing a fresh install.

## Example application: Dockerising glusterd

As an example of how this can be useful, consider glusterd, the server daemon for
the distributed glusterfs file system. Glusterd has a concept of trusted user pools -
any other systems you 'probe' into the pool (which is necessary to link those
servers to combine storage) has effectively full access to create new 'volumes' that
allow read and write access to anywhere on the host. In nearly all circumstances, this
is likely to be enough to lead to escalation of privileges from root on any computer
in the trusted user pool to root access on all systems in the pool. For this reason,
you might like to run glusterd in a filesystem namespace.

In addition, glusterd puts a lot of trust in the network (although it does provide
TLS options) - and so you may like to run glusterd in a network namespace.

For these reason, you might like to run glusterd inside Docker. However, glusterd
heavily uses xattrs in the `trusted` namespace for its own internal attribute data.
You could run the container with the `CAP_SYS_ADMIN` priviledge, or worse as
`--privileged`. However, these options make it easy to escape the container and
gain root access on the host - and given the Gluster security model, you might find
it prudent to avoid giving the container such options.

You can use fake-trusted-xattr to map glusterd's xattr names to the `user` namespace,
which doesn't need any special permissions to read and write.

When building a Docker image Glusterd, build the binary from this repo, and add lines
like the following to your Dockerfile:

```
COPY libfake_trusted_xattr.so /usr/lib/libfake_trusted_xattr.so
ENV LD_PRELOAD=/usr/lib/libfake_trusted_xattr.so
```

## Building the shared object

You will need git and Docker installed to use the build script:
```
git clone https://gitlab.com/A1kmm/fake-trusted-xattr
cd fake-trusted-xattr
scripts/build
# Optional - test your binary
scripts/test
```

This will create a `libfake_trusted_xattr.so` file in the top-level directory. You
can load it by setting `LD_PRELOAD=/path/to/libfake_trusted_xattr.so` (adjusting
the path as required).
