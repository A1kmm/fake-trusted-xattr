#!/bin/bash -e

FAILURE=0
setfattr -n trusted.test -v "a" test
if [[ $(getfattr --only-values -n trusted.test test) != "a" ]];
then
    FAILURE=1
    echo -n "❌[Set and read back #1]"
else
    echo -n "✅"
fi

setfattr -n trusted.test -v "b" test
if [[ $(getfattr --only-values -n trusted.test test) != "b" ]];
then
    FAILURE=1
    echo -n "❌[Set and read back #2]"
else
    echo -n "✅"
fi

getfattr -m ".*" test >/tmp/fattr
diff -q test/expected-present /tmp/fattr > /dev/null && RET=$? || RET=$?
if [[ $RET != 0 ]];
then
    FAILURE=1
    echo -n "❌[List when should exist]"
else
    echo -n "✅"
fi

setfattr -x trusted.test test
getfattr -m ".*" test >/tmp/fattr
diff -q test/expected-absent /tmp/fattr > /dev/null && RET=$? || RET=$?
if [[ $RET != 0 ]];
then
    FAILURE=1
    echo -n "❌[List when should not exist]"
else
    echo -n "✅"
fi

echo
echo -n Test completed
if [[ $FAILURE == 0 ]]; then
    echo " successfully"
else
    echo " with failure"
fi
exit $FAILURE
